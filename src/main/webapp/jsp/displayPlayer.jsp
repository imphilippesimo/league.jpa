<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Affichage d'un joueur</title>
<link type="text/css" rel="stylesheet" href="css/style.css" />

</head>

<body>
	<%-- Affichage de la chaîne "message" transmise par la
servlet --%>
	<p class="info">${ message }</p>
	<%-- Puis affichage des données enregistrées enregistrées dans la base transmis par la servlet --%>
	<p>Nom : ${ player.name }</p>
	<p>Prénom : ${ player.firstName }</p>
	<p>Dossard : ${ player.jerseyNumber }</p>
	<p>Statut : ${ player.status }</p>
	<p>Salaire : ${ player.salary }</p>
</body>
</html>