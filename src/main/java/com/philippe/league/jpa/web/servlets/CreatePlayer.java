package com.philippe.league.jpa.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.philippe.league.jpa.dao.GeneralDAO;
import com.philippe.league.jpa.entity.Player;

public class CreatePlayer extends HttpServlet {

	GeneralDAO<Player> dao = GeneralDAO.getInstance();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// récupération des données saisies
		String name = req.getParameter("name");
		String firstName = req.getParameter("firstName");
		String status = req.getParameter("status");
		Double salary = Double.valueOf(req.getParameter("salary"));
		int jerseyNumber = Integer.valueOf(req.getParameter("jerseyNumber"));
		
	
		

		Player player = new Player(name, firstName, jerseyNumber, null, status, null, salary);
		// appel du modèle
		player = dao.create(player);
		
		System.out.println("Ok modification");
		
		System.out.println(player.toString());

		req.setAttribute("player", player);

		// renvoi à la vue
		this.getServletContext().getRequestDispatcher("/jsp/displayPlayer.jsp").forward(req, resp);
		

	}

}
