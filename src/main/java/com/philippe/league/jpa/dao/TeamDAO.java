package com.philippe.league.jpa.dao;

import java.util.Collection;

import com.philippe.league.jpa.entity.Team;

public class TeamDAO extends GeneralDAO<Team> {
	
	private static TeamDAO instance = null;

	private TeamDAO() {

	}

	public static TeamDAO getInstance() {
		if (instance != null)
			return instance;

		synchronized (TeamDAO.class) {
			instance = new TeamDAO();
		}
		return instance;

	}

	public Collection<Team> findLike(String matchString) {

		String jpql = "SELECT t FROM Team t where t.name LIKE :mt OR t.division LIKE :mt OR t.city LIKE :mt";
		return em.createQuery(jpql).setParameter("mt", matchString).getResultList();

	}
	
	public Collection<Team> findAll() {

		String jpql = "SELECT t FROM Team t ";
		return em.createQuery(jpql).getResultList();

	}

}
